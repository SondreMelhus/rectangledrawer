import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RectangleDrawer {

    public static void drawRectangle() throws InputException {

        //Regex pattern that checks if a given string contains 1 or more digits
        Pattern onlyDigits = Pattern.compile("[0-9]*");

        Scanner input = new Scanner(System.in);

        //Get the height of the rectangle from the user
        System.out.print("Type the height of the rectangle: ");
        String stringSizeX = input.next();

        //Get the width of the rectangle from the user
        System.out.print("Type the width of the rectangle: ");
        String stringSizeY = input.next();

        //Add the two inputs to the pattern matcher that checks that the strings only contain digits
        Matcher matcher1 = onlyDigits.matcher(stringSizeX);
        Matcher matcher2 = onlyDigits.matcher(stringSizeY);

        //Throws exception if they contain anything else than a digit
        if (!matcher1.matches() || !matcher2.matches()) {
            throw new InputException("You have to input a number");
        }

        //Parse the input strings to integers
        int sizeX = Integer.valueOf(stringSizeX);
        int sizeY = Integer.valueOf(stringSizeY);

        //Get the symbol the user wants to use to write the rectangle
        System.out.print("Type the symbol you want: ");
        String symbol = input.next();
        System.out.println();


        // Used to iterate through each coordinate in the canvas
        for(int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                // Defines the constraints for when we should draw a symbol, and when to leave the space empty
                if (j == 0 || j == sizeY - 1 || i == 0 || i == sizeX - 1) {
                    System.out.print(" " + symbol + " ");
                } else {
                    System.out.print("   ");
                }
            }
            // Used to shift one line down when we have finished drawing a line
            System.out.println();
        }
    }

    public static void main(String[] args) {

        try {
            RectangleDrawer.drawRectangle();
        } catch (InputException e) {
            System.out.println(e);
        }

    }
}
